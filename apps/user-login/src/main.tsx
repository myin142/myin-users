import { AuthService } from '@myin-users/authentication';
import React from 'react';
import ReactDOM from 'react-dom';

import App from './app/app';

const auth = new AuthService();

ReactDOM.render(
  <React.StrictMode>
    <App auth={auth} />
  </React.StrictMode>,
  document.getElementById('root')
);
