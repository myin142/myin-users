import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { Login } from '@myin-users/login';
import { UserPage } from '@myin-users/user-page';
import { AuthService } from '@myin-users/authentication';

import { environment } from '../environments/environment';

function GuardedRoute(condition: () => boolean, elsePath: string) {
  return function ({ component: Component, ...rest }) {
    return (
      <Route
        {...rest}
        render={(props) =>
          condition() ? <Component {...props} /> : <Redirect to={elsePath} />
        }
      />
    );
  };
}

export interface AppProps {
  auth: AuthService;
}

export default class App extends React.Component<AppProps> {
  constructor(props: AppProps) {
    super(props);
  }

  private isAuthenticated(): boolean {
    return this.props.auth.isAuthenticated();
  }

  render() {
    const AuthRoute = GuardedRoute(() => this.isAuthenticated(), '/login');
    const LoginRoute = GuardedRoute(() => !this.isAuthenticated(), '/');

    return (
      <BrowserRouter basename={environment.baseHref}>
        <Switch>
          <Route exact path="/">
            <Redirect to="/user"></Redirect>
          </Route>
          <LoginRoute
            path="/login"
            component={(props) => <Login auth={this.props.auth} {...props} />}
          ></LoginRoute>
          <AuthRoute path="/user" component={() => <UserPage />}></AuthRoute>
        </Switch>
      </BrowserRouter>
    );
  }
}
