import React from 'react';
import { AuthService } from '@myin-users/authentication';
import { Redirect } from 'react-router-dom';

import './login.scss';

export interface LoginProps {
  auth: AuthService;
}

export interface LoginState {
  username: string;
  password: string;
}

export class Login extends React.Component<LoginProps, LoginState> {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
    };
  }

  private updateUsername(ev: React.ChangeEvent<HTMLInputElement>) {
    const username = ev.target.value;
    this.setState({ username });
  }

  private updatePassword(ev: React.ChangeEvent<HTMLInputElement>) {
    const password = ev.target.value;
    this.setState({ password });
  }

  private async login() {
    await this.props.auth.login(this.state.username, this.state.password);
    this.setState({
      username: '',
      password: '',
    });
  }

  render() {
    if (this.props.auth.isAuthenticated()) {
      return <Redirect to="/" />;
    }
    return (
      <div>
        <input
          placeholder="Username"
          onChange={this.updateUsername.bind(this)}
        />
        <input
          placeholder="Password"
          type="password"
          onChange={this.updatePassword.bind(this)}
        />
        <button onClick={this.login.bind(this)}>Login</button>
      </div>
    );
  }
}
