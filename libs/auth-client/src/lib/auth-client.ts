import { CognitoRefreshToken, CognitoUser, CognitoUserPool, CognitoUserSession } from 'amazon-cognito-identity-js';

export class AuthClient {
	public static TOKEN_KEY = 'myin-users-login-token';
	public static USERNAME_KEY = 'myin-users-username';
	public static REFRESH_TOKEN_KEY = 'myin-users-refresh-token';
	public static REDIRECT_KEY = 'myin-users-redirect-key';

	private static LOGIN_URL = '/myin-users';

	private userPool: CognitoUserPool;

	constructor(private redirect = false, private debug = false) {
		this.userPool = new CognitoUserPool({
			UserPoolId: 'eu-central-1_9D7QfJapc',
			ClientId: '3rj8no2dsf53o2jkdqm0l7kdfc',
		});
	}

	get token(): string {
		return localStorage.getItem(AuthClient.TOKEN_KEY);
	}

	private get username(): string {
		return localStorage.getItem(AuthClient.USERNAME_KEY);
	}

	private get refreshToken(): string {
		return localStorage.getItem(AuthClient.REFRESH_TOKEN_KEY);
	}

	getCognitoUser(username: string = this.username): CognitoUser {
		return new CognitoUser({
			Username: username,
			Pool: this.userPool,
		});
	}

	setSession(username: string, session: CognitoUserSession) {
		localStorage.setItem(AuthClient.TOKEN_KEY, `Bearer ${session.getIdToken().getJwtToken()}`);
		localStorage.setItem(AuthClient.USERNAME_KEY, username);
		localStorage.setItem(AuthClient.REFRESH_TOKEN_KEY, session.getRefreshToken().getToken());
	}

	logout() {
		this.clearSession();
		this.redirectLogin();
	}

	isAuthenticated(): boolean {
		return !!this.token;
	}

	private redirectLogin() {
		if (this.redirect) {
			localStorage.setItem(AuthClient.REDIRECT_KEY, window.location.href);
			window.location.href = AuthClient.LOGIN_URL;
		} else if (this.debug) {
			console.log(`Redirecting to ${AuthClient.LOGIN_URL}`);
		}
	}

	private clearSession() {
		localStorage.removeItem(AuthClient.TOKEN_KEY);
		localStorage.removeItem(AuthClient.USERNAME_KEY);
		localStorage.removeItem(AuthClient.REFRESH_TOKEN_KEY);
	}

	async refreshSession(clearOnFailure = true): Promise<boolean> {
		return new Promise(resolve => {
			this.getCognitoUser().refreshSession(new CognitoRefreshToken({ RefreshToken: this.refreshToken }),
				(err, session) => {
					if (err && clearOnFailure) {
						this.logout();
						resolve(false);
						return;
					}
					this.setSession(this.username, session);
					resolve(true);
				});
		});
	}
}