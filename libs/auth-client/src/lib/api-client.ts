import axios, { AxiosRequestConfig } from 'axios';
import { Path } from '@myin/utils';
import { AuthClient } from './auth-client';

export class ApiClient {
  private tokenFn: () => string;
  private baseURL: string;
  private authClient: AuthClient;
  private debug: boolean;
  private expiredCallback: () => void;

  constructor({
    baseURL,
    authClient,
    debug = false,
    stage = Stage.LOCAL,
    tokenFn = () => '',
    expiredCallback = () => {},
  }: ApiSetup) {
    this.baseURL = this.createApiUrl(baseURL, stage);
    this.tokenFn = tokenFn;
    this.authClient = authClient;
    this.debug = debug;
    this.expiredCallback = expiredCallback;
  }

  private createApiUrl(url: string, stage: Stage): string {
    stage = stage || Stage.LOCAL;

    if (stage.startsWith('http://')) {
      url = stage;
    } else {
      url = Path.join(url, stage);
    }

    return url;
  }

  async get<T>(url: string): Promise<T> {
    return this.request({ url, method: 'GET' });
  }

  async post<T, R>(url: string, data: T = null): Promise<R> {
    return this.request({ url, method: 'POST', data });
  }

  private async request<T>(req: AxiosRequestConfig): Promise<T> {
    req.baseURL = this.baseURL;

    // Interceptors too difficult to test
    if (this.tokenFn && this.tokenFn()) {
      if (!req.headers) req.headers = {};
      req.headers.Authorization = this.tokenFn();
    }

    try {
      const response = await axios.request(req);
      return response.data;
    } catch (err) {
      if (err.response == null && err.message === 'Network Error') {
        console.log('Refresh Unknown', err.reponse);
        await this.authClient.refreshSession(true);
        this.expiredCallback();
        throw err;
      }
      if (err.response != null && err.response.status === 401) {
        console.log('Refresh Unauthorized', err.reponse);
        await this.authClient.refreshSession(err.response);
        this.expiredCallback();
        throw err;
      }
    }
  }
}

export enum Stage {
  PROD = 'prod',
  LOCAL = 'http://localhost:3000', // Default api gateway url using SAM local start-api
}

export interface ApiSetup {
  /**
   * Specify the stage of the api gateway
   * If stage is LOCAL then localhost will be used
   */
  stage?: Stage;

  /**
   * Base URL of api requests
   */
  baseURL: string;

  /**
   * Function to get authorization token, required for authenticated requests
   */
  tokenFn?: () => string;

  /**
   * Auth client used to redirect to login
   */
  authClient: AuthClient;

  /**
   * Enable debug logging
   */
  debug?: boolean;

  /**
   * Called when session has expired and was refreshed
   */
  expiredCallback?: () => void;
}
