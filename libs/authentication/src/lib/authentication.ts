import { AuthClient } from '@myin-users/auth-client';
import { AuthenticationDetails } from 'amazon-cognito-identity-js';
import * as AWS from 'aws-sdk/global';

AWS.config.region = 'eu-central-1';

export class AuthService {

	private authClient: AuthClient;

	constructor() {
		this.authClient = new AuthClient();
	}

	async login(username: string, password: string): Promise<boolean> {
		var authenticationDetails = new AuthenticationDetails({
			Username: username,
			Password: password,
		});

		return new Promise(resolve => {
			this.authClient.getCognitoUser(username).authenticateUser(authenticationDetails, {
				onSuccess: result => {
					this.authClient.setSession(username, result);
					resolve(true);
				},
				onFailure: err => {
					console.log('Login Failed', err);
					resolve(false);
				},
			});
		});
	}

	isAuthenticated(): boolean {
		return !!this.getToken();
	}

	getToken(): string {
		return this.authClient.token;
	}
}
