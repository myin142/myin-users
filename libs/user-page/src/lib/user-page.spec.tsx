import React from 'react';
import { render } from '@testing-library/react';

import UserPage from './user-page';

describe('UserPage', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<UserPage />);
    expect(baseElement).toBeTruthy();
  });
});
