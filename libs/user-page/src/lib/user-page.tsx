import React from 'react';
import { AuthClient } from '@myin-users/auth-client';

import './user-page.scss';

/* eslint-disable-next-line */
export interface UserPageProps {}

export const UserPage = (props: UserPageProps) => {
  const redirect = localStorage.getItem(AuthClient.REDIRECT_KEY);
  if (redirect) {
    window.location.href = redirect;
  }

  return (
    <div>
      <h1>Welcome to user-page!</h1>
    </div>
  );
};

export default UserPage;
